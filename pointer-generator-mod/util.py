# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
# Modifications Copyright 2017 Abigail See
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""This file contains some utility functions"""

import tensorflow as tf
import numpy as np
import time
import os

from scipy.stats import truncnorm

FLAGS = tf.app.flags.FLAGS


def get_truncated_normal(mean=0, sd=1e-4, low=-2, upp=2):
    return truncnorm(
        (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)


def get_config():
    """Returns config for tf.session"""
    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    return config


def load_ckpt(saver, sess, ckpt_dir="train"):
    """Load checkpoint from the ckpt_dir (if unspecified, this is train dir) and restore it to saver and sess, waiting 10 secs in the case of failure. Also returns checkpoint name."""
    while True:
        try:
            latest_filename = "checkpoint_best" if ckpt_dir == "eval" else None
            ckpt_dir = os.path.join(FLAGS.log_root, ckpt_dir)
            ckpt_state = tf.train.get_checkpoint_state(ckpt_dir, latest_filename=latest_filename)
            tf.logging.info('Loading checkpoint %s', ckpt_state.model_checkpoint_path)
            saver.restore(sess, ckpt_state.model_checkpoint_path)
            return ckpt_state.model_checkpoint_path
        except:
            tf.logging.info("Failed to load checkpoint from %s. Sleeping for %i secs...", ckpt_dir, 10)
            time.sleep(10)


def _load_GloVe(filename, data_vocab):
    embd = []

    glove_dict = {}

    file = open(filename, 'r')
    for line in file.readlines():
        row = line.strip().split(' ')
        word = row[0]
        vector = row[1:]
        glove_dict[word] = vector
    file.close()
    print('Loaded GloVe!')
    emb_dim = len(vector)

    for word_id in range(data_vocab.size()):
        word = data_vocab.id2word(word_id)
        if glove_dict.get(word):
            embd.append(glove_dict.get(word))
        else:
            embd.append(get_truncated_normal().rvs(emb_dim))
    return embd


def get_GloVe_embbeddings(path, data_vocab):
    embd = _load_GloVe(path, data_vocab)
    embedding = np.asarray(embd)

    return embedding
