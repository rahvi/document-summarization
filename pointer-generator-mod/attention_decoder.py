# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
# Modifications Copyright 2017 Abigail See
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""This file defines the decoder"""

import tensorflow as tf
from tensorflow.python.ops import variable_scope
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import nn_ops
from tensorflow.python.ops import math_ops


# Note: this function is based on tf.contrib.legacy_seq2seq_attention_decoder, which is now outdated.
# In the future, it would make more sense to write variants on the attention mechanism using the new seq2seq library for tensorflow 1.0: https://www.tensorflow.org/api_guides/python/contrib.seq2seq#Attention
def attention_decoder(decoder_inputs, initial_state, encoder_states, enc_padding_mask, cell,
                      initial_state_attention=False, pointer_gen=True, use_coverage=False, prev_coverage=None,
                      use_temporal=False, prev_scores=None, use_intra_attention=True, prev_dec_states=None, multihead=False, head_context=False):
    """
    Args:
      decoder_inputs: A list of 2D Tensors [batch_size x input_size].
      initial_state: 2D Tensor [batch_size x cell.state_size].
      encoder_states: 3D Tensor [batch_size x attn_length x attn_size].
      enc_padding_mask: 2D Tensor [batch_size x attn_length] containing 1s and 0s; indicates which of the encoder locations are padding (0) or a real token (1).
      cell: rnn_cell.RNNCell defining the cell function and size.
      initial_state_attention:
        Note that this attention decoder passes each decoder input through a linear layer with the previous step's context vector to get a modified version of the input.
        If initial_state_attention is False, on the first decoder step the "previous context vector" is just a zero vector.
        If initial_state_attention is True, we use initial_state to (re)calculate the previous step's context vector.
        We set this to False for train/eval mode (because we call attention_decoder once for all decoder steps) and True for decode mode (because we call attention_decoder once for each decoder step).
      pointer_gen: boolean. If True, calculate the generation probability p_gen for each decoder step.
      use_coverage: boolean. If True, use coverage mechanism.
      prev_coverage:
        If not None, a tensor with shape (batch_size, attn_length). The previous step's coverage vector. This is only not None in decode mode when using coverage.

    Returns:
      outputs: A list of the same length as decoder_inputs of 2D Tensors of
        shape [batch_size x cell.output_size]. The output vectors.
      state: The final state of the decoder. A tensor shape [batch_size x cell.state_size].
      attn_dists: A list containing tensors of shape (batch_size,attn_length).
        The attention distributions for each decoder step.
      p_gens: List of length input_size, containing tensors of shape [batch_size, 1]. The values of p_gen for each decoder step. Empty list if pointer_gen=False.
      coverage: Coverage vector on the last step computed. None if use_coverage=False.
    """
    with variable_scope.variable_scope("attention_decoder") as scope:
        batch_size = encoder_states.get_shape()[0].value  # if this line fails, it's because the batch size isn't defined
        attn_size = encoder_states.get_shape()[2].value  # if this line fails, it's because the attention length isn't defined

        # Reshape encoder_states (need to insert a dim)
        encoder_states = tf.expand_dims(encoder_states, axis=2)  # now is shape (batch_size, attn_len, 1, attn_size)

        # To calculate attention, we calculate
        #   v^T tanh(W_h h_i + W_s s_t + b_attn)
        # where h_i is an encoder state, and s_t a decoder state.
        # attn_vec_size is the length of the vectors v, b_attn, (W_h h_i) and (W_s s_t).
        # We set it to be equal to the size of the encoder states.
        attention_vec_size = attn_size

        # Get the weight matrix W_h and apply it to each encoder state to get (W_h h_i), the encoder features
        W_h = variable_scope.get_variable("W_h", [1, 1, attn_size, attention_vec_size])
        encoder_features = nn_ops.conv2d(encoder_states, W_h, [1, 1, 1, 1],"SAME")  # shape (batch_size,attn_length,1,attention_vec_size)

        # Get the weight vectors v and w_c (w_c is for coverage)
        v = variable_scope.get_variable("v", [attention_vec_size])
        if use_coverage:
            with variable_scope.variable_scope("coverage"):
                w_c = variable_scope.get_variable("w_c", [1, 1, 1, attention_vec_size])

        if use_intra_attention:
            with variable_scope.variable_scope("intra_attention"):
                # Get the weight matrix w_d and apply it to each encoder state to get (W_h h_i), the encoder features
                w_d = variable_scope.get_variable("w_d", [1, 1, attn_size, attention_vec_size])

        if prev_coverage is not None:  # for beam search mode with coverage
            # reshape from (batch_size, attn_length) to (batch_size, attn_len, 1, 1)
            prev_coverage = tf.expand_dims(tf.expand_dims(prev_coverage, 2), 3)

        def attention(decoder_state, coverage=None, scores=None, prev_dec_states=None):
            """Calculate the context vector and attention distribution from the decoder state.

            # (bs, 1, enc)

            Args:
              decoder_state: state of the decoder
              coverage: Optional. Previous timestep's coverage vector, shape (batch_size, attn_len, 1, 1).

            Returns:
              context_vector: weighted sum of encoder_states
              attn_dist: attention distribution
              coverage: new coverage vector. shape (batch_size, attn_len, 1, 1)
            """
            with variable_scope.variable_scope("Attention"):
                # Pass the decoder state through a linear layer (this is W_s s_t + b_attn in the paper)
                org_decoder_features = linear(decoder_state, attention_vec_size, True)  # shape (batch_size, attention_vec_size)
                decoder_features = tf.expand_dims(tf.expand_dims(org_decoder_features, 1), 1)  # reshape to (batch_size, 1, 1, attention_vec_size)

                def masked_attention(e):
                    """Take softmax of e then apply enc_padding_mask and re-normalize"""
                    attn_dist = nn_ops.softmax(e)  # take softmax. shape (batch_size, attn_length)
                    attn_dist *= enc_padding_mask  # apply mask
                    masked_sums = tf.reduce_sum(attn_dist, axis=1)  # shape (batch_size)
                    return attn_dist / tf.reshape(masked_sums, [-1, 1])  # re-normalize

                def masked_attention_temporal(e_prime):
                    """Take softmax of e then apply enc_padding_mask and re-normalize"""
                    attn_dist = tf.div(e_prime, tf.reduce_sum(e_prime, axis=1, keep_dims=True))  # (bs, enc)
                    attn_dist *= enc_padding_mask  # apply mask
                    masked_sums = tf.reduce_sum(attn_dist, axis=1)  # shape (batch_size)
                    return attn_dist / tf.reshape(masked_sums, [-1, 1])  # re-normalize

                if use_intra_attention and prev_dec_states is not None:
                    # Calculate v^T tanh(W_h h_i + W_s s_t + b_attn)
                    prev_decoder_features = nn_ops.conv2d(tf.expand_dims(prev_dec_states, axis=2), w_d, [1, 1, 1, 1],"SAME")  # shape (batch_size,prev_dec_len,1,attention_vec_size)
                    e_decoder = math_ops.reduce_sum(v * math_ops.tanh(prev_decoder_features + decoder_features),[2, 3])  # calculate e (bs, prev_decoder)
                    attn_dist_decoder = nn_ops.softmax(e_decoder) # (bs, prev_decoder)

                    # Update the scores and the prev dec_states
                    dec_states = tf.concat([prev_dec_states, tf.expand_dims(org_decoder_features,axis=1)], axis=1) # (bs, dec, dec_hidden) TODO: is this the right dec sate in lstm tuple? might be the dec output should be needed instead?

                if use_coverage and coverage is not None:  # non-first step of coverage
                    # Multiply coverage vector by w_c to get coverage_features.
                    coverage_features = nn_ops.conv2d(coverage, w_c, [1, 1, 1, 1], "SAME")  # c has shape (batch_size, attn_length, 1, attention_vec_size)

                    # Calculate v^T tanh(W_h h_i + W_s s_t + w_c c_i^t + b_attn)
                    e = math_ops.reduce_sum(v * math_ops.tanh(encoder_features + decoder_features + coverage_features), [2, 3])  # shape (batch_size,attn_length)

                    # Calculate attention distribution
                    attn_dist = masked_attention(e)

                    # Update coverage vector
                    coverage += array_ops.reshape(attn_dist, [batch_size, -1, 1, 1])

                elif use_temporal and scores is not None:
                    # Calculate v^T tanh(W_h h_i + W_s s_t + b_attn)
                    e = math_ops.reduce_sum(v * math_ops.tanh(encoder_features + decoder_features), [2, 3])  # calculate e (bs, enc)
                    e_max = tf.reduce_max(scores, axis=1)  # Making the softmax numerical stable (bs, enc)
                    e_prime = tf.exp(e - e_max) / tf.reduce_sum(tf.exp(scores - tf.expand_dims(e_max, axis=1)), axis=1) # (bs, enc)

                    # Update the scores
                    scores = tf.concat([scores, tf.expand_dims(e, axis=1)], axis=1)  # (bs, dec, enc)

                    attn_dist = masked_attention_temporal(e_prime)
                else:

                    if multihead:
                        num_heads = 8

                        Q = tf.expand_dims(org_decoder_features, axis=1) # (bs, 1, 1, num_hidden) we can also get a (bs, num_hidden) size if needed.
                        K = tf.squeeze(encoder_features, axis=2) # (bs,enc_len,1,num_hidden)

                        # Linear projection
                        Q_proj = tf.layers.dense(Q, attn_size, activation=tf.nn.relu)
                        K_proj = tf.layers.dense(K, attn_size, activation=tf.nn.relu)

                        # Multi-heads
                        Q_heads = split_heads(Q_proj, num_heads) # (bs, heads, 1, hidden/heads)
                        K_heads = split_heads(K_proj, num_heads) # (bs, heads, enc, hidden/heads)
                        assert Q_heads.get_shape() == (batch_size, num_heads, 1, attn_size/num_heads)
                        assert K_heads.get_shape()[:-2] == (batch_size, num_heads)

                        # Scaled dotproduct
                        logits = tf.matmul(Q_heads, K_heads, transpose_b=True)
                        scalin_factor = attn_size // num_heads
                        logits *= scalin_factor ** -0.5
                        weights = tf.nn.softmax(logits, name="attention_weights")
                        assert weights.get_shape()[:-2] == (batch_size, num_heads)

                        # Masked attention similar to masked_attention()
                        weights *= tf.tile(tf.expand_dims(tf.expand_dims(enc_padding_mask, axis=1), axis=2), multiples=(1,num_heads,1,1))  # apply mask
                        masked_sums = tf.reduce_sum(weights, axis=3)  # shape (batch_size)
                        weights =  weights / tf.expand_dims(masked_sums, axis=3) # re-normalize (bs, heads, 1, enc_len)
                        assert weights.get_shape()[:-2] == (batch_size, num_heads)

                        # Calculating context vector
                        attentions = tf.expand_dims(tf.expand_dims(tf.squeeze(weights, axis=2), axis=3), axis=4) # (bs, heads, enc_len, 1, 1)
                        encoding_states = tf.tile(tf.expand_dims(encoder_states, axis=1), multiples=(1,num_heads,1,1,1)) # (bs, heads, enc_len, 1, hidden)

                        if head_context:
                            encoding_heads = tf.expand_dims(split_heads(K, num_heads), axis=3) # (bs, heads, enc, 1, hidden/heads)
                            context_vec = tf.matmul(attentions, encoding_heads)
                            context_vec_sum = tf.reduce_sum(context_vec, axis=(2, 3))  # (bs, heads, hidden)

                            context_vec_size = attn_size/num_heads
                            assert context_vec_sum.get_shape() == (batch_size, num_heads, context_vec_size)
                        else:
                            context_vec = tf.matmul(attentions, encoding_states) # (bs, heads, enc_len, 1, hidden)
                            context_vec_sum = tf.reduce_sum(context_vec, axis=(2,3)) # (bs, heads, hidden)
                            context_vec_size = attn_size
                            assert context_vec_sum.get_shape() == (batch_size, num_heads, context_vec_size)

                        # combine heads
                        final_context_vec = combine_last_two_dimensions(context_vec_sum) # shape()
                        assert final_context_vec.get_shape() == (batch_size, context_vec_size * 8)

                        # Linear projection of heads
                        final_context_vec_proj = tf.layers.dense(final_context_vec, attn_size, use_bias=False, name="output_transform")
                        assert final_context_vec_proj.get_shape() == (batch_size, attn_size)

                        return final_context_vec_proj, tf.squeeze(weights, axis=2)[:, 0, :], None, None

                    # Calculate v^T tanh(W_h h_i + W_s s_t + b_attn)
                    e = math_ops.reduce_sum(v * math_ops.tanh(encoder_features + decoder_features), [2, 3])  # calculate e

                    # Calculate attention distribution
                    attn_dist = masked_attention(e) # (bs, enc)

                    if use_coverage:  # first step of training
                        coverage = tf.expand_dims(tf.expand_dims(attn_dist, 2), 2)  # initialize coverage
                    if use_temporal:
                        scores = tf.expand_dims(e, axis=1) # (bs, 1, enc)
                    if use_intra_attention and prev_dec_states is None:
                        # The decoder_state is a Tuple, so we take the first one which is the hidden state
                        # according to https://www.tensorflow.org/api_docs/python/tf/contrib/rnn/LSTMStateTuple
                        dec_states = tf.expand_dims(org_decoder_features, axis=1) # (bs, 1, dec_hidden)

                # Calculate the context vector from attn_dist and encoder_states
                context_vector = math_ops.reduce_sum(tf.reshape(attn_dist, [batch_size, -1, 1, 1]) * encoder_states, [1, 2])  # shape (batch_size, attn_size).
                context_vector = tf.reshape(context_vector, [-1, attn_size])

                if use_intra_attention:
                    if prev_dec_states is None: # Then we are in the first step and we set the decoder context vec to zero as in Paulus
                        context_vector_decoder = tf.zeros((batch_size, attn_size))
                    else:
                        context_vector_decoder = math_ops.reduce_sum(array_ops.reshape(attn_dist_decoder, [batch_size, -1, 1, 1]) * tf.expand_dims(prev_dec_states, axis=2), [1, 2])  # shape (batch_size, attn_size).
                        context_vector_decoder = array_ops.reshape(context_vector_decoder, [-1, attn_size])

                    return context_vector, context_vector_decoder, attn_dist, coverage, scores, dec_states

            return context_vector, attn_dist, coverage, scores

        outputs = []
        attn_dists = []
        p_gens = []
        state = initial_state
        coverage = prev_coverage  # initialize coverage to None or whatever was passed in
        scores = prev_scores # initialize scores to None or whatever was passed in
        dec_states = prev_dec_states # initialize dec_states to None or whatever was passed in

        context_vector = array_ops.zeros([batch_size, attn_size])
        context_vector.set_shape([None, attn_size])  # Ensure the second shape of attention vectors is set.
        if use_intra_attention:
            context_vecs = [context_vector] + [context_vector]
        else:
            context_vecs = [context_vector]
        if initial_state_attention:  # true in decode mode
            # Re-calculate the context vector from the previous step so that we can pass it through a linear layer with this step's input to get a modified version of the input
            if use_intra_attention:
                context_vector, context_vector_decoder, attn_dist, coverage, scores, dec_states = attention(initial_state,
                                                                                                            coverage,
                                                                                                            scores,
                                                                                                            dec_states)
                context_vecs = [context_vector] + [context_vector_decoder]
            else:
                context_vector, _, coverage, scores = attention(initial_state, coverage, scores, dec_states)  # in decode mode, this is what updates the coverage vector
                context_vecs = [context_vector]
        for i, inp in enumerate(decoder_inputs):
            tf.logging.info("Adding attention_decoder timestep %i of %i", i, len(decoder_inputs))
            if i > 0:
                variable_scope.get_variable_scope().reuse_variables()

            # Merge input and previous attentions into one vector x of the same size as inp
            input_size = inp.get_shape().with_rank(2)[1]
            if input_size.value is None:
                raise ValueError("Could not infer input size from input: %s" % inp.name)
            x = linear([inp] + context_vecs, input_size, True)

            # Run the decoder RNN cell. cell_output = decoder state
            cell_output, state = cell(x, state)

            # Run the attention mechanism.
            if i == 0 and initial_state_attention:  # always true in decode mode
                with variable_scope.variable_scope(variable_scope.get_variable_scope(),reuse=True):  # you need this because you've already run the initial attention(...) call
                    if use_intra_attention:
                        context_vector, context_vector_decoder, attn_dist, _, _, _ = attention(
                            state,
                            coverage,
                            scores,
                            dec_states)
                        context_vecs = [context_vector] + [context_vector_decoder]
                    else:
                        context_vector, attn_dist, _, _ = attention(state, coverage, scores, dec_states)  # don't allow coverage to update
                        context_vecs = [context_vector]
            else:
                if use_intra_attention:
                    context_vector, context_vector_decoder, attn_dist, coverage, scores, dec_states = attention(state,
                                                                                                                coverage,
                                                                                                                scores,
                                                                                                                dec_states)
                    context_vecs = [context_vector] + [context_vector_decoder]
                    assert context_vector.shape == (batch_size, attention_vec_size)
                    assert context_vector_decoder.shape == (batch_size, attention_vec_size)
                    assert dec_states.shape[1] == i + 1
                    if use_temporal:
                        assert scores.shape[1] == i + 1 # The scores shape accumulates each timestep
                else:
                    context_vector, attn_dist, coverage, scores = attention(state, coverage, scores)
                    context_vecs = [context_vector]

            attn_dists.append(attn_dist)

            # Calculate p_gen
            if pointer_gen:
                with tf.variable_scope('calculate_pgen'):
                    if use_intra_attention:
                        p_gen = linear([context_vector, state.c, state.h, x, context_vector_decoder], 1, True)  # Tensor shape (batch_size, 1)
                    else:
                        p_gen = linear([context_vector, state.c, state.h, x], 1, True)  # Tensor shape (batch_size, 1)
                    p_gen = tf.sigmoid(p_gen)
                    p_gens.append(p_gen)

            # Concatenate the cell_output (= decoder state) and the context vector, and pass them through a linear layer
            # This is V[s_t, h*_t] + b in the paper
            with variable_scope.variable_scope("AttnOutputProjection"):
                output = linear([cell_output] + context_vecs, cell.output_size, True) # (bs, num_hidden)
            outputs.append(output)

        # If using coverage, reshape it
        if coverage is not None:
            coverage = array_ops.reshape(coverage, [batch_size, -1])

        return outputs, state, attn_dists, p_gens, coverage, scores, dec_states


def linear(args, output_size, bias, bias_start=0.0, scope=None):
    """Linear map: sum_i(args[i] * W[i]), where W[i] is a variable.

    Args:
      args: a 2D Tensor or a list of 2D, batch x n, Tensors.
      output_size: int, second dimension of W[i].
      bias: boolean, whether to add a bias term or not.
      bias_start: starting value to initialize the bias; 0 by default.
      scope: VariableScope for the created subgraph; defaults to "Linear".

    Returns:
      A 2D Tensor with shape [batch x output_size] equal to
      sum_i(args[i] * W[i]), where W[i]s are newly created matrices.

    Raises:
      ValueError: if some of the arguments has unspecified or wrong shape.
    """
    if args is None or (isinstance(args, (list, tuple)) and not args):
        raise ValueError("`args` must be specified")
    if not isinstance(args, (list, tuple)):
        args = [args]

    # Calculate the total size of arguments on dimension 1.
    total_arg_size = 0
    shapes = [a.get_shape().as_list() for a in args]
    for shape in shapes:
        if len(shape) != 2:
            raise ValueError("Linear is expecting 2D arguments: %s" % str(shapes))
        if not shape[1]:
            raise ValueError("Linear expects shape[1] of arguments: %s" % str(shapes))
        else:
            total_arg_size += shape[1]

    # Now the computation.
    with tf.variable_scope(scope or "Linear"):
        matrix = tf.get_variable("Matrix", [total_arg_size, output_size])
        if len(args) == 1:
            res = tf.matmul(args[0], matrix)
        else:
            res = tf.matmul(tf.concat(axis=1, values=args), matrix)
        if not bias:
            return res
        bias_term = tf.get_variable(
            "Bias", [output_size], initializer=tf.constant_initializer(bias_start))
    return res + bias_term


def split_heads(x, num_heads):
    """Split channels (dimension 2) into multiple heads (becomes dimension 1).
    Args:
      x: a Tensor with shape [batch, length, channels]
      num_heads: an integer
    Returns:
      a Tensor with shape [batch, num_heads, length, channels / num_heads]
    """
    return tf.transpose(split_last_dimension(x, num_heads), [0, 2, 1, 3])


def split_last_dimension(x, n):
    """Reshape x so that the last dimension becomes two dimensions.
    The first of these two dimensions is n.
    Args:
      x: a Tensor with shape [..., m]
      n: an integer.
    Returns:
      a Tensor with shape [..., n, m/n]
    """
    x_shape = shape_list(x)
    m = x_shape[-1]
    if isinstance(m, int) and isinstance(n, int):
        assert m % n == 0
    return tf.reshape(x, x_shape[:-1] + [n, m // n])


def shape_list(x):
    """Return list of dims, statically where possible."""
    x = tf.convert_to_tensor(x)

    # If unknown rank, return dynamic shape
    if x.get_shape().dims is None:
        return tf.shape(x)

    static = x.get_shape().as_list()
    shape = tf.shape(x)

    ret = []
    for i in xrange(len(static)):
        dim = static[i]
        if dim is None:
            dim = shape[i]
        ret.append(dim)
    return ret


def combine_heads(x):
    """Inverse of split_heads.
    Args:
      x: a Tensor with shape [batch, num_heads, length, channels / num_heads]
    Returns:
      a Tensor with shape [batch, length, channels]
    """
    return combine_last_two_dimensions(tf.transpose(x, [0, 2, 1, 3]))


def combine_last_two_dimensions(x):
    """Reshape x so that the last two dimension become one.
    Args:
      x: a Tensor with shape [..., a, b]
    Returns:
      a Tensor with shape [..., ab]
    """
    x_shape = shape_list(x)
    a, b = x_shape[-2:]
    return tf.reshape(x, x_shape[:-2] + [a * b])



# # N = bs
# # T = maxlen
# # C = num_hidden
#
# def multihead_attention(queries,
#                         keys,
#                         num_units=None,
#                         num_heads=8,
#                         dropout_rate=0,
#                         is_training=True,
#                         causality=False,
#                         scope="multihead_attention",
#                         reuse=None):
#     '''Applies multihead attention.
#
#     Args:
#       queries: A 3d tensor with shape of [N, T_q, C_q].
#       keys: A 3d tensor with shape of [N, T_k, C_k].
#       num_units: A scalar. Attention size.
#       dropout_rate: A floating point number.
#       is_training: Boolean. Controller of mechanism for dropout.
#       causality: Boolean. If true, units that reference the future are masked.
#       num_heads: An int. Number of heads.
#       scope: Optional scope for `variable_scope`.
#       reuse: Boolean, whether to reuse the weights of a previous layer
#         by the same name.
#
#     Returns
#       A 3d tensor with shape of (N, T_q, C)
#     '''
#     with tf.variable_scope(scope, reuse=reuse):
#         # Set the fall back option for num_units
#         if num_units is None:
#             num_units = queries.get_shape().as_list[-1]
#
#         # Linear projections
#         Q = tf.layers.dense(queries, num_units, activation=tf.nn.relu)  # (N, T_q, C)
#         K = tf.layers.dense(keys, num_units, activation=tf.nn.relu)  # (N, T_k, C)
#         V = tf.layers.dense(keys, num_units, activation=tf.nn.relu)  # (N, T_k, C)
#
#         # Split and concat
#         Q_ = tf.concat(tf.split(Q, num_heads, axis=2), axis=0)  # (h*N, T_q, C/h)
#         K_ = tf.concat(tf.split(K, num_heads, axis=2), axis=0)  # (h*N, T_k, C/h)
#         V_ = tf.concat(tf.split(V, num_heads, axis=2), axis=0)  # (h*N, T_k, C/h)
#
#         # Multiplication
#         outputs = tf.matmul(Q_, tf.transpose(K_, [0, 2, 1]))  # (h*N, T_q, T_k)
#
#         # Scale
#         outputs = outputs / (K_.get_shape().as_list()[-1] ** 0.5)
#
#         # Key Masking
#         key_masks = tf.sign(tf.abs(tf.reduce_sum(keys, axis=-1)))  # (N, T_k)
#         key_masks = tf.tile(key_masks, [num_heads, 1])  # (h*N, T_k)
#         key_masks = tf.tile(tf.expand_dims(key_masks, 1), [1, tf.shape(queries)[1], 1])  # (h*N, T_q, T_k)
#
#         paddings = tf.ones_like(outputs) * (-2 ** 32 + 1)
#         outputs = tf.where(tf.equal(key_masks, 0), paddings, outputs)  # (h*N, T_q, T_k)
#
#         # Causality = Future blinding
#         if causality:
#             diag_vals = tf.ones_like(outputs[0, :, :])  # (T_q, T_k)
#             tril = tf.contrib.linalg.LinearOperatorTriL(diag_vals).to_dense()  # (T_q, T_k)
#             masks = tf.tile(tf.expand_dims(tril, 0), [tf.shape(outputs)[0], 1, 1])  # (h*N, T_q, T_k)
#
#             paddings = tf.ones_like(masks) * (-2 ** 32 + 1)
#             outputs = tf.where(tf.equal(masks, 0), paddings, outputs)  # (h*N, T_q, T_k)
#
#         # Activation
#         outputs = tf.nn.softmax(outputs)  # (h*N, T_q, T_k)
#
#         # Query Masking
#         query_masks = tf.sign(tf.abs(tf.reduce_sum(queries, axis=-1)))  # (N, T_q)
#         query_masks = tf.tile(query_masks, [num_heads, 1])  # (h*N, T_q)
#         query_masks = tf.tile(tf.expand_dims(query_masks, -1), [1, 1, tf.shape(keys)[1]])  # (h*N, T_q, T_k)
#         outputs *= query_masks  # broadcasting. (N, T_q, C)
#
#         # Dropouts
#         outputs = tf.layers.dropout(outputs, rate=dropout_rate, training=tf.convert_to_tensor(is_training))
#
#         # Weighted sum
#         outputs = tf.matmul(outputs, V_)  # ( h*N, T_q, C/h)
#
#         # Restore shape
#         outputs = tf.concat(tf.split(outputs, num_heads, axis=0), axis=2)  # (N, T_q, C)
#
#         # Residual connection
#         outputs += queries
#
#         # Normalize
#         outputs = normalize(outputs)  # (N, T_q, C)
#
#
#         return outputs