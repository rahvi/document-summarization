# Document summarization using deep learning

## Setup

1. Get the data:
run ./data.sh

2. Setup python, Tensorflow with dependencies, CUDA and nvidia drivers:
cd setup
run ./gpu-setup-part1.sh

reconnect to ssh(server is rebooted)

run ./gpu-setup-part2.sh

verity everything works:
python gpu-test.py


## Setup a port forward through SSH for Tensorboard.
```ssh -N -f -L localhost:16006:localhost:6006 <user@remote>```
```ssh -L 16006:127.0.0.1:6006 user@remote```

## Fix locale setting
This problem sometimes occour on the servers. Fixed in this Stack Overflow post
https://stackoverflow.com/questions/14547631/python-locale-error-unsupported-locale-setting

```export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo dpkg-reconfigure locales```

## DTU Compute GPU
To mount your own drive use following command from
http://www.gbar.dtu.dk/faq/78-home-directory
```sshfs username@transfer.gbar.dtu.dk: local-directory```
To dismount use this command. If it will not dismount then use the
```fusermount -uz local-directory```


## Transfer files between instances
Transfer files from one instance remote1 to another remote2
https://stackoverflow.com/questions/28831329/how-to-transfer-a-file-between-two-remote-servers-using-scp-from-a-third-local
```ssh -A -t user1@remote1 scp -r srcpath user2@remote2:destpath ```

## Google Cloud GPU drivers
https://cloud.google.com/compute/docs/gpus/add-gpus#install-driver-script