#!/bin/bash

sudo apt install perl libdb5.3-dev libdb5.3
cpan App::cpanminus
source "$HOME/.bashrc"
cpanm XML::DOM --force # maybe use --force

git clone https://github.com/andersjo/pyrouge.git

ROUGE_ROOT="$HOME/document-summarization/setup/pyrouge/tools/ROUGE-1.5.5"
WORDNET_DB="$ROUGE_ROOT/data/WordNet-2.0.exc.db"

echo $WORDNET_DB
echo $ROUGE_ROOT/data/WordNet-2.0-Exceptions/buildExeptionDB.pl

pip install pyrouge
pyrouge_set_rouge_path $ROUGE_ROOT
rm $WORDNET_DB
$ROUGE_ROOT/data/WordNet-2.0-Exceptions/buildExeptionDB.pl $ROUGE_ROOT/data/WordNet-2.0-Exceptions db $WORDNET_DB
#mv $HOME_DIR/WordNet-2.0.exc.db $WORDNET_DB

python test_rouge.py
