from pyrouge import Rouge155
import os

r = Rouge155()
os.mkdir('system')
os.mkdir('reference')

with open("reference/reference.1.txt","w") as f:
   f.write("the cat was under the bed")

with open("system/system.1.txt","w") as f:
   f.write("the cat was found under the bed")

r.system_dir = 'system'
r.model_dir = 'reference'
r.system_filename_pattern = 'system.(\d+).txt'
r.model_filename_pattern = 'reference.#ID#.txt'

output = r.convert_and_evaluate()
print(output)
output_dict = r.output_to_dict(output)

os.remove("reference/reference.1.txt")
os.remove("system/system.1.txt")
os.rmdir("system")
os.rmdir("reference")
