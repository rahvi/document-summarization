#!/bin/bash

#Get CNN/Daily dataset

wget -O Data.zip https://www.dropbox.com/s/5hervw468rnb3dp/finished_files.zip?dl=1

sudo apt-get install unzip

unzip Data.zip

mv $(pwd)/finished_files $(pwd)/data

rm Data.zip

#Get word embeddings

wget http://nlp.stanford.edu/data/glove.6B.zip

unzip glove.6B.zip

mv glove.6B.100d.txt $(pwd)/data/glove.6B.100d.txt

rm glove.6B.zip

rm glove.6B.50d.txt glove.6B.200d.txt glove.6B.300d.txt
