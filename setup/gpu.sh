CUDA_REPO_PKG="cuda-repo-ubuntu1604_8.0.61-1_amd64.deb"
wget -O /tmp/${CUDA_REPO_PKG} http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/${CUDA_REPO_PKG}
sudo dpkg -i /tmp/${CUDA_REPO_PKG}
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
rm -f /tmp/${CUDA_REPO_PKG}

sudo apt-get update
sudo apt-get install cuda-8-0

# install cuDNN v6.0
CUDNN_TAR_FILE="cudnn-8.0-linux-x64-v6.0.tgz"
wget -O /tmp/${CUDNN_TAR_FILE} http://developer.download.nvidia.com/compute/redist/cudnn/v6.0/${CUDNN_TAR_FILE}
tar -xzvf /tmp/${CUDNN_TAR_FILE} --directory /tmp/
sudo cp -P /tmp/cuda/include/cudnn.h /usr/local/cuda-8.0/include
sudo cp -P /tmp/cuda/lib64/libcudnn* /usr/local/cuda-8.0/lib64/
sudo chmod a+r /usr/local/cuda-8.0/lib64/libcudnn*

echo >> $HOME/.bashrc '
export CUDNN_LIB_DIR=/usr/local/cuda-8.0/lib64
export CUDNN_INCLUDE_DIR=/usr/local/cuda-8.0/include
export LD_LIBRARY_PATH=$CUDNN_LIB_DIR:$CUDNN_INCLUDE_DIR:$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/cuda

export PATH="/usr/local/cuda/bin:$PATH"
'

sudo ln -s /usr/local/cuda-8.0/lib64/libcudnn.so.6.0.21 /usr/local/cuda-8.0/lib64/libcudnn.so.6.0