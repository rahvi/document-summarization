# update bashrc
echo "Updating bashrc"

echo >> $HOME/.bashrc '
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
'

source $HOME/.bashrc