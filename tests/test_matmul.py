import numpy as np
import tensorflow as tf
from numpy.testing import assert_allclose

num_hidden = 2
enc_len = 4
dec_len = 3
bs=1

#W_attn = np.array([[1,4],[3,2]])
W_attn = np.random.uniform(low=-0.5, high=0.5, size=(num_hidden,num_hidden))

#h_e = np.array([[5,2],[7,9],[3,4],[2,1]])
#h_d = np.array([[6,4],[8,3],[9,7]])
h_e = np.array([np.random.uniform(low=0, high=1, size=(num_hidden,)) for x in range(enc_len)])
h_d = np.array([np.random.uniform(low=0, high=1, size=(num_hidden,)) for x in range(dec_len)])

######################
# NumPy calculations #
######################
e = [] # (dec, enc)
for t in range(dec_len):
    e.append([])
    for i in range(enc_len):
        e_ti = np.matmul(np.matmul(h_d[t].T, W_attn), h_e[i])
        e[t].append(e_ti)

e_alternative = np.matmul(np.matmul(h_d, W_attn), h_e.T)#np.reshape(h_e, (num_hidden, enc_len)))

###########################
# TensorFlow calculations #
###########################
h_d_tf = tf.placeholder(tf.float32, shape=(bs, dec_len, num_hidden))
w_att_e = tf.placeholder(tf.float32, shape=(num_hidden, num_hidden))
h_e_tf = tf.placeholder(tf.float32, shape=(bs, enc_len, num_hidden))

#e_prod_tf = tf.einsum('ijk,kl->ijl', h_d_tf, w_att_e)
e_prod_tf = tf.matmul(h_d_tf, tf.expand_dims(w_att_e, axis=0))
e_res_tf = tf.matmul(e_prod_tf, tf.transpose(h_e_tf, perm=[0, 2, 1]))

sess = tf.Session()
sess.run(tf.global_variables_initializer())

W_attn_tf, e_tf = sess.run([w_att_e, e_res_tf], feed_dict={
    h_d_tf: np.expand_dims(h_d, axis=0), # Expanding dims to give a batch dim
    h_e_tf: np.expand_dims(h_e, axis=0), # Expanding dims to give a batch dim
    w_att_e: W_attn
})

########################
# Testing calculations #
########################
assert_allclose(W_attn, W_attn_tf, rtol=1e-06)
assert_allclose(e, np.squeeze(e_tf, axis=0),rtol=1e-06)
assert_allclose(e, e_alternative, rtol=1e-06)
assert_allclose(e_alternative, np.squeeze(e_tf, axis=0), rtol=1e-06)

print("Everything is fine!")