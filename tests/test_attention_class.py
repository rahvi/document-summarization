from collections import namedtuple

import numpy as np
import tensorflow as tf

from textsum.attention import Attention

HParams = namedtuple('HParams','batch_size use_temporal_attention use_coverage scoring_function dec_timesteps enc_timesteps num_hidden use_decoder_attention')
hps = HParams(
    batch_size = 64,
    use_temporal_attention=False,
    use_coverage = False,
    use_decoder_attention = False,
    scoring_function = 'bahdanau',
    dec_timesteps = 3,
    enc_timesteps=4,
    num_hidden = 200
)


h_e = np.random.uniform(low=0, high=1, size=(hps.batch_size, hps.enc_timesteps, hps.num_hidden))
h_d = np.random.uniform(low=0, high=1, size=(hps.batch_size, hps.dec_timesteps, hps.num_hidden))

h_e_tf = tf.placeholder(tf.float32, shape=(hps.batch_size, hps.enc_timesteps, hps.num_hidden))
h_d_tf = tf.placeholder(tf.float32, shape=(hps.batch_size, hps.dec_timesteps, hps.num_hidden))

def test_attention():
    attn_layer = Attention(h_d_tf, h_e_tf, hps)
    context_vec = attn_layer.compute_context_vecs()

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    if hps.use_decoder_attention:
        c_v, c_d = sess.run(context_vec, feed_dict={h_e_tf: h_e, h_d_tf: h_d})
        return (c_v.shape == (hps.batch_size, hps.dec_timesteps, hps.num_hidden)) and (c_d.shape == (hps.batch_size, hps.dec_timesteps, hps.num_hidden))
    else:
        c_v = sess.run(context_vec, feed_dict={h_e_tf: h_e, h_d_tf: h_d})
        return (c_v.shape == (hps.batch_size, hps.dec_timesteps, hps.num_hidden))

print("Shapes correct......: "+str(test_attention()))