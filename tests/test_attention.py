import numpy as np
import tensorflow as tf
from numpy.testing import assert_allclose

from attention import attention

num_hidden = 200

enc_len = 4
dec_len = 3

# h_e = [np.random.uniform(low=-0.5, high=0.5, size=(num_hidden,)) for x in range(enc_len)]
# h_d = [np.random.uniform(low=-0.5, high=0.5, size=(num_hidden,)) for x in range(dec_len)]
h_e = [np.random.uniform(low=0, high=1, size=(num_hidden,)) for x in range(enc_len)]
h_d = [np.random.uniform(low=0, high=1, size=(num_hidden,)) for x in range(dec_len)]

#W_attn = np.random.uniform(low=0, high=1, size=(num_hidden,num_hidden))
w_e = np.random.uniform(low=-0.5, high=0.5, size=(num_hidden, num_hidden))

w_d = np.random.uniform(low=-0.5, high=0.5, size=(num_hidden, num_hidden))


#h_e = [np.random.random((num_hidden)) for x in range(enc_len)]
#h_d = [np.random.random((num_hidden)) for x in range(dec_len)]
#W_attn = np.random.random((num_hidden, num_hidden))

def temporal_attention_with_numpy(h_e, h_d, W_attn):
    ##############################
    # Calculate e as formula (2) #
    ##############################
    e = [] # (dec, enc)
    for t in range(dec_len):
        e.append([])
        for i in range(enc_len):
            e_ti = np.matmul(np.matmul(h_d[t].T, W_attn), h_e[i])
            e[t].append(e_ti)

    # Dimension check
    assert(len(e) == dec_len)
    for t in range(dec_len):
        assert(len(e[t]) == enc_len)

    #static_e_max = np.max(e)

    ###############################
    # Calculate e' as formula (3) #
    ###############################
    e_prime = [] # (dec, enc)
    for t in range(dec_len):
        e_prime.append([])
        for i in range(enc_len):
            if t == 0:
                e_prime[t].append(np.exp(e[t][i]))
            else:
                prev_ei = np.array([prev_e[i] for prev_e in e[:t]])
                e_max = np.max([prev_e[i] for prev_e in e[:t]])
                numerator = np.exp(e[t][i] - e_max)
                prev_t_sum = np.exp(prev_ei - e_max)
                denominator = np.sum(prev_t_sum)
                res = numerator / denominator
                e_prime[t].append(res)

    # Dimension check
    assert(len(e_prime) == dec_len)
    for t in range(dec_len):
        assert(len(e_prime[t]) == enc_len)

    # Infinity check
    assert(np.isinf(e_prime).all() == False)

    #########################################
    # Nomalize e' into alpha as formula (4) #
    #########################################
    alpha = []
    for t in range(dec_len):
        alpha.append(e_prime[t] / np.sum(e_prime[t]))

    # Dimension check
    assert(len(alpha) == dec_len)
    for t in range(dec_len):
        assert(len(alpha[t]) == enc_len)

    ###########################################
    # Calculate context vector as formula (5) #
    ###########################################
    c_e = []
    for t in range(dec_len):
        temp_res = []
        for i in range(enc_len):
            temp_res.append(alpha[t][i] * h_e[i])
        c_e.append(np.sum(temp_res, axis=0))

    # Dimension check
    assert(len(c_e) == dec_len)
    for t in range(dec_len):
        assert(len(c_e[t]) == num_hidden)

    return e, e_prime, alpha, c_e

def decoder_attention_with_numpy(h_d, w_d):
    e_d = []
    for t in range(dec_len):
        e_d.append([])
        for t_prime in range(t):
            e_d[t].append(np.matmul(np.matmul(h_d[t].T, w_d), h_d[t_prime]))

    alpha_d = []
    for t in range(dec_len):
        if t ==0:
            alpha_d.append([])
        else:
            max_val = max(e_d[t])
            alpha_d.append(np.exp(e_d[t]-max_val) / np.sum(np.exp(e_d[t]-max_val)))

    c_d = []
    for t in range(dec_len):
        temp_res = []
        for t_prime in range(t):
            temp_res.append(alpha_d[t][t_prime] * h_d[t_prime])
        if t == 0:
            c_d.append(np.zeros(shape=(num_hidden,)))
        else:
            c_d.append(np.sum(temp_res, axis=0))

    assert(len(c_d) == dec_len)
    for t in range(dec_len):
        assert(len(c_d[t]) == num_hidden)

    return c_d

def attention_with_tf(h_e, h_d, w_e, w_d):
    bs = 1
    outputs_dec = tf.placeholder(tf.float32, shape=(bs, dec_len, num_hidden)) # h_d

    w_att_e = tf.placeholder(tf.float32, shape=(num_hidden, num_hidden))
    w_att_d = tf.placeholder(tf.float32, shape=(num_hidden, num_hidden))

    h_e_tf = tf.placeholder(tf.float32, shape=(bs, num_hidden, enc_len))
    e_t_i, e_t_prime, a_t_i, c_e_t, c_d_t = attention(bs, dec_len, num_hidden, outputs_dec, h_e_tf, w_att_e, w_att_d, verbose=True)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    e_tf, e_prime_tf, alpha_tf, c_e_tf, c_d_tf = sess.run([e_t_i, e_t_prime, a_t_i, c_e_t, c_d_t], feed_dict={
        outputs_dec: np.expand_dims(np.array(h_d), axis=0),
        h_e_tf: np.expand_dims(np.array(h_e).T, axis=0),
        w_att_e: w_e,
        w_att_d: w_d
    })

    # Check dimension on context vectors from TensorFlow Code
    assert (len(c_e_tf) == bs)  # bs
    assert (len(c_e_tf[0]) == dec_len)
    for t in range(dec_len):
        assert (len(c_e_tf[0][t]) == num_hidden)

    return e_tf, e_prime_tf, alpha_tf, c_e_tf, c_d_tf


########################
# Testing calculations #
########################
e_np, e_prime_np, alpha_np, c_e_np = temporal_attention_with_numpy(h_e, h_d, w_e)
c_d_np = decoder_attention_with_numpy(h_d, w_d)
e_tf, e_prime_tf, alpha_tf, c_e_tf, c_d_tf = attention_with_tf(h_e, h_d, w_e, w_d)

tolerance = 1e-04
assert_allclose(e_np, np.squeeze(e_tf, axis=0), rtol=tolerance)                     # e
assert_allclose(e_prime_np, np.squeeze(e_prime_tf, axis=0), rtol=tolerance)         # e'
assert_allclose(np.array(alpha_np).T, np.squeeze(alpha_tf, axis=0), rtol=tolerance) # alpha
assert_allclose(c_e_np, np.squeeze(c_e_tf, axis=0), rtol=tolerance)                   # c_e

assert_allclose(c_d_np, np.squeeze(c_d_tf, axis=0), rtol=tolerance)               # c_d

print("Everything is fine!")