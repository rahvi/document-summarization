import os
from nltk import ngrams
from collections import Counter
import numpy as np

folder = "/home/hvingelby/Workspace/thesis/results/see-mod-decoding/pointer-intra-pretrained-coverage/decode_test_400maxenc_4beam_35mindec_100maxdec_ckpt-287000/decoded"
results = [[], [], [], [], []] # 1-, 2-, 3- and 4-grams and sentence

ns = range(1,5)
num_files = 0
for file in os.listdir(folder):
    with open(folder+"/"+file, 'r') as f:
        decoded_string = f.read().replace('\n',' ')

        sentences = decoded_string.split(' . ')
        sentences_counter = Counter(sentences)

        num_total_sentences = len(sentences)
        num_duplicate_sentences = 0
        for key, value in dict(sentences_counter).items():
            if value > 1:
                num_duplicate_sentences += value-1

        results[4].append(float(num_duplicate_sentences) / float(num_total_sentences))


        decoded_string = decoded_string.replace(' . ', '').replace(' .', '')

        for n in ns:
            n_grams = ngrams(decoded_string.split(), n)
            n_grams = list(n_grams)

            counter = Counter(n_grams)

            num_total_n_grams = len(n_grams)
            num_unique_n_grams = len(counter)
            num_duplicate_n_grams = 0
            for key, value in dict(counter).items():
                if value > 1:
                    num_duplicate_n_grams += value-1
                    #num_duplicate_n_grams += 1

            #results[n-1].append(float(num_duplicate_n_grams) / float(num_unique_n_grams))
            results[n - 1].append(float(num_duplicate_n_grams) / float(num_total_n_grams+0.000001))


    num_files += 1

print("1-gram duplicate percentage: {}".format(np.round(np.mean(results[0])*100, 3)))
print("2-gram duplicate percentage: {}".format(np.round(np.mean(results[1])*100, 3)))
print("3-gram duplicate percentage: {}".format(np.round(np.mean(results[2])*100, 3)))
print("4-gram duplicate percentage: {}".format(np.round(np.mean(results[3])*100, 3)))
print("Sentences duplicate percentage: {}".format(np.round(np.mean(results[4])*100, 3)))
print(num_files)