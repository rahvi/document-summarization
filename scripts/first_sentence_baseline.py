# TODO: loop through the test.txt file
import os

import pyrouge


def split(txt, seps):
    default_sep = seps[0]

    # we skip seps[0] because that's the default seperator
    for sep in seps[1:]:
        txt = txt.replace(sep, default_sep)
    return [i.strip() for i in txt.split(default_sep)]


def make_html_safe(s):
    """Replace any angled brackets in string s to avoid interfering with HTML attention visualizer."""
    s.replace("<", "&lt;")
    s.replace(">", "&gt;")
    return s

def write_for_rouge(reference_sents, decoded_sents, ex_index):

    # pyrouge calls a perl script that puts the data into HTML files.
    # Therefore we need to make our output HTML safe.
    decoded_sents = [make_html_safe(w) for w in decoded_sents]
    reference_sents = [make_html_safe(w) for w in reference_sents]

    # Write to file
    ref_file = os.path.join(rouge_ref_dir, "%06d_reference.txt" % ex_index)
    decoded_file = os.path.join(rouge_dec_dir, "%06d_decoded.txt" % ex_index)

    with open(ref_file, "w") as f:
        for idx, sent in enumerate(reference_sents):
            f.write(sent) if idx == len(reference_sents) - 1 else f.write(sent + "\n")
    with open(decoded_file, "w") as f:
        for idx, sent in enumerate(decoded_sents):
            f.write(sent) if idx == len(decoded_sents) - 1 else f.write(sent + "\n")

def rouge_eval(ref_dir, dec_dir):
  """Evaluate the files in ref_dir and dec_dir with pyrouge, returning results_dict"""
  r = pyrouge.Rouge155()
  r.model_filename_pattern = '#ID#_reference.txt'
  r.system_filename_pattern = '(\d+)_decoded.txt'
  r.model_dir = ref_dir
  r.system_dir = dec_dir
  rouge_results = r.convert_and_evaluate()
  return r.output_to_dict(rouge_results)

rouge_ref_dir = "ref"
rouge_dec_dir = "dec"

os.makedirs(rouge_ref_dir)
os.makedirs(rouge_dec_dir)

dm_single_close_quote = u'\u2019' # unicode
dm_double_close_quote = u'\u201d'
END_TOKENS = ['.', '!', '?', '...', dm_single_close_quote, dm_double_close_quote, ")"] # acceptable ways to end a sentence

test_file = "/home/hvingelby/Workspace/thesis/random-data/finished_files/test.txt"

counter = 0
with open(test_file, 'r') as file:
    for line in file:
        article_sents = line.split('abstract=b')[0].replace('article=b', '')
        splitted_article_sents = split(article_sents, END_TOKENS)
        first_dec_sent = splitted_article_sents[0] + " . "
        second_dec_sent = splitted_article_sents[1] + " . "
        third_dec_sent = splitted_article_sents[2] + " . "

        abstract_sents = line.split('abstract=b')[1].split("</s>")
        abstract_sents = [abstract.replace("<s>", '') for abstract in abstract_sents if "\n" not in abstract]

        write_for_rouge(abstract_sents, [first_dec_sent, second_dec_sent, third_dec_sent], counter) # write ref summary and decoded summary to file, to eval with pyrouge later
        counter += 1 # this is how many examples we've decoded

print("Decoded: "+str(counter)+" instances")
print(rouge_eval(rouge_ref_dir, rouge_dec_dir))