import os
import glob
import subprocess

meteor_location = '/home/hvingelby/Workspace/thesis/document-summarization/setup/meteor-1.5/meteor-1.5.jar'

decode_folder = 'decoded'
reference_folder = 'reference'

decoded_file = 'decodes.txt'
reference_file = 'references.txt'

dec_filelist = glob.glob(decode_folder+"/*.txt")
for dec_file in sorted(dec_filelist):
    with open(dec_file, 'r') as d_f:
        with open(decoded_file, "a") as single_d_f:
            data=d_f.read().replace('\n', ' ')
            single_d_f.write(str(data)+"\n")

ref_filelist = glob.glob(reference_folder+"/*.txt")
for ref_file in sorted(ref_filelist):
    with open(ref_file, 'r') as r_f:
        with open(reference_file, "a") as single_r_f:
            data=r_f.read().replace('\n', ' ')
            single_r_f.write(str(data)+"\n")

# Evaluate full METEOR
command = "java -Xmx2G -jar "+meteor_location+" decodes.txt references.txt -l en -norm"
process = subprocess.Popen(command, stdout = subprocess.PIPE, shell = True)
(process_output,  error) = process.communicate()

with open("res_meteor_full", "w") as res_file:
    res_file.write(process_output)

# Evaluate exact METEOR
command = "java -Xmx2G -jar "+meteor_location+" decodes.txt references.txt -l en -norm -m 'exact'"
process = subprocess.Popen(command, stdout = subprocess.PIPE, shell = True)
(process_output,  error) = process.communicate()

with open("res_meteor_exact", "w") as res_file:
    res_file.write(process_output)
