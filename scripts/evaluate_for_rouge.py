from pyrouge import Rouge155
import os

ref_file = "/home/hvingelby/Workspace/thesis/server-experiments/results/decode_monday_server/ref1509570587"
dec_file = "/home/hvingelby/Workspace/thesis/server-experiments/results/decode_monday_server/decode1509570587"

#os.mkdir('system')
#os.mkdir('reference')

#line_idx = set()

#with open(ref_file) as r_f:
#    seen = set()
#    duplicate = set()
#    for i, line in enumerate(r_f):
#        line_lower = line.lower()
#        if line_lower in seen:
#            duplicate.add(line_lower)
#        else:
#            line_idx.add(i)
#            seen.add(line_lower)
#            with open("reference/reference.{}.txt".format(i),"w") as single_r_f:
#                single_r_f.write(line.replace('output=', ''))

#with open(dec_file) as d_f:
#    for i, line in enumerate(d_f):
#        if i in line_idx:
#            with open("system/system.{}.txt".format(i),"w") as single_d_f:
#                single_d_f.write(line.replace('output=', ''))

#print(len(line_idx))
#print(len(seen))
#print(len(duplicate))
#print("Hello")

r = Rouge155()
r.system_dir = 'decoded'
r.model_dir = 'reference'
r.model_filename_pattern = '#ID#_reference.txt'
r.system_filename_pattern = '(\d+)_decoded.txt'

output = r.convert_and_evaluate()
print(output)
output_dict = r.output_to_dict(output)

