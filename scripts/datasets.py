import numpy as np

# input_sets = {
#     'duc2003_input' : "/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2003/input.txt",
#     'duc2004_input' : "/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2004/input.txt",
#     'gigaword_input' : "/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/train/train.article.txt/data"
# }
#
# for name, path in input_sets.items():
#     word_count = []
#     with open(path, 'r') as file:
#         for line in file:
#             words = line.replace(' . ', '').replace(' .', '').split(' ')
#             words_len = len(words)
#             word_count.append(words_len)
#
#     print("{} avg #words: {}".format(name, np.average(word_count)))
#
# gigaword_titles = '/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/train/train.title.txt/data'
# word_count = []
# sent_count = []
# with open(gigaword_titles, 'r') as file:
#     for line in file:
#         words = line.replace(' . ', '').replace(' .', '').split(' ')
#         words_len = len(words)
#         word_count.append(words_len)
#
#         sents = line.split(' . ')
#         sent_count.append(len(sents))
# print("gigaword titles #words: {}".format(np.mean(word_count)))
# print("gigaword titles #sents: {}".format(np.mean(sent_count)))


titles = []
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2003/task1_ref0.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2003/task1_ref1.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2003/task1_ref2.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2003/task1_ref3.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2004/task1_ref0.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2004/task1_ref1.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2004/task1_ref2.txt")
titles.append("/home/hvingelby/Workspace/Foreign/li_emnlp_2017/summary/sumdata/DUC2004/task1_ref3.txt")

word_count = []
sent_count = []
for path in titles:
    with open(path, 'r') as file:
        for line in file:
            words = line.replace(' . ', '').replace(' .', '').split(' ')
            words_len = len(words)
            word_count.append(words_len)

            sents = line.split(' . ')
            sent_count.append(len(sents))

print("duc titles #words: {}".format(np.mean(word_count)))
print("duc titles #sents: {}".format(np.mean(sent_count)))