import numpy as np
import pickle

number_of_refs = 10490

ref_list = np.random.permutation(number_of_refs)
refs = number_of_refs / 2
rasmus_list = ref_list[:refs]
mathias_list = ref_list[refs:]

with open('rasmus_list', 'wb') as f:
    pickle.dump(rasmus_list, f)


with open('mathias_list', 'wb') as f:
    pickle.dump(mathias_list, f)


with open('mathias_list', 'rb') as f:
    indices_list = pickle.load(f)

print len(indices_list)
