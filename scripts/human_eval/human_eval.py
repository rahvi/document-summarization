# -*- coding: utf-8 -*-

# Eksempel #007599 for documentation
#
# Guidelines for evaluation
#
# # Information
# Skala: 1-5
# reference er ground truth. man kan circa tælle facts i reference og se om de bliver dækket i kandidat.
# vær hård mod modellen.
#
# # Readability
# Skala: 1-5
# Volapyk og gentagelser trækker ned.
# Dårlig grammatik trækker ned.
# Contraditions i forhold til sig selv trækker ned.
# Information i overenstemmelse med reference påvirker ikke vurdering.
#
# # Faithfullness
# Skala: TRUE/FALSE
# Siger kandidaten noget som der er et modstridende faktum i forhold til referencen?


import numpy as np
import os
import re
import pandas as pd
import pickle
from glob import glob

def fix_index(idx):
    str_idx = idx
    while len(str_idx) < 6:
        str_idx = '0' + str_idx

    return str_idx

def print_summary(model_path, number, reference=False):
    if reference:
        sum_type = '/reference/'
    else:
        sum_type = '/decoded/'

    summary_path = model_path + sum_type

    test = [f for f in os.listdir(summary_path) if os.path.isfile(os.path.join(summary_path, f)) and number in f]
    if test:
        with open(summary_path + test[0]) as f:
            summary = f.readlines()

        for line in summary: print line.replace("\n", "")
    else:
        print("No summary for:" + summary_path + number)

    return

#################
### SET THESE ###
#################

model_base_path = '/home/hvingelby/Workspace/thesis/results/see-mod-decoding/'
idx_filename = 'rasmus_list'

#These should align
model_names = ['pointer-intra-pretrained-coverage', 'pointer-pretrained', 'pointer_pretrained_coverage', 'pointer-temporal-pretrained']
model_checkpoints = ['287000', '260000', '360000', '93931']


final_model_paths = []
for i, model in enumerate(model_names):
    for check_point_folder in os.listdir(model_base_path + model):
        if model_checkpoints[i] in check_point_folder:
            final_model_paths.append(model_base_path + model + "/" + check_point_folder)


some_ref_path = glob(final_model_paths[1] + '/reference')[0]
ref_filenames = [f for f in os.listdir(some_ref_path) if os.path.isfile(os.path.join(some_ref_path, f))]
ref_numbers = [re.findall(r'\d+', ref)[0] for ref in ref_filenames if re.findall(r'\d+', ref)]

print("Found {} references".format(len(ref_numbers)))


indices_list = []

with open(idx_filename, 'rb') as f:
    indices_list = pickle.load(f)

string_indices_list = [fix_index(str(idx)) for idx in indices_list]

print("Loaded list of {} indices".format(len(string_indices_list)))

if os.path.isfile("results.csv"):
    result_df = pd.read_csv("results.csv", index_col=0)
    indices_covered = [fix_index(str(idx)) for idx in result_df.index.values]
    print("Found results for {} summaries".format(len(indices_covered)))
else:
    indices_covered = []
    result_df = pd.DataFrame()

uncovered_indices = [idx for idx in string_indices_list if idx not in indices_covered]
print("{} remaining".format(len(uncovered_indices)))

print('\n--------------\n')
print('Rate summaries on readability, informativeness and faithfulness')
print('Readability and informativeness are scored from 1-5 where 5 is the best')
print('Faithfullness is measured true or false with "y" or "n"')
print('\n--------------\n')
raw_input('Press any button to begin!')

os.system('cls' if os.name == 'nt' else 'clear')

while uncovered_indices:
    print("{} remaining\n".format(len(uncovered_indices)))

    rnd_reference = uncovered_indices.pop(np.random.randint(len(uncovered_indices)))
    rnd_model_selection = np.random.permutation(len(final_model_paths))

    print('####################')
    print('# Reference-' + rnd_reference + ' #')
    print('####################\n')
    print_summary(final_model_paths[1], rnd_reference, reference=True)
    print('\n--------------\n')

    tmp_dict = {}

    for model_idx in rnd_model_selection:
        print_summary(final_model_paths[model_idx], rnd_reference)
        print('\n')
        readability_score = int(raw_input('Enter readability score(1-5):'))
        semantic_score = int(raw_input('Enter informativeness score(1-5):'))
        faithful_score = raw_input('Enter faithfulness(y/n):')
        faithful_score = 1 if faithful_score == 'y' else 0

        scores = [readability_score, semantic_score, faithful_score]

        tmp_dict[model_names[model_idx]] = scores
        print('\n--------------\n')

    experiment = {}
    experiment[rnd_reference] = tmp_dict

    tmp_df = pd.DataFrame.from_dict(experiment, orient='index')

    result_df = result_df.append(tmp_df)

    result_df.to_csv("results.csv")
    raw_input("All models evalulated and results are saved.... Next?")
    os.system('cls' if os.name == 'nt' else 'clear')
