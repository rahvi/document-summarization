word_vecs_home = "/home/hvingelby/Workspace/thesis/wordvecs"

dataset_vocab_file = "/home/hvingelby/Workspace/thesis/document-summarization/setup/data/vocab"

#word_embedding_vocab_file = word_vecs_home + "/glove.6B/glove.6B.100d.txt"
#word_embedding_vocab_file = word_vecs_home + "/glove.42B.300d.txt"
#word_embedding_vocab_file = word_vecs_home + "/glove.840B.300d.txt"
#word_embedding_vocab_file = word_vecs_home + "/numberbatch-en-17.06.txt/data"
#word_embedding_vocab_file = word_vecs_home + "/wiki.en/wiki.en.vec"
#word_embedding_vocab_file =  word_vecs_home + "/GoogleNews-vectors-negative300.bin/data.txt"
word_embedding_vocab_file =  "/home/hvingelby/Workspace/thesis/fastText/modelvecs.vec"

word_embedding_dict = {}



with open(word_embedding_vocab_file, "r") as word_embedding_vocab:
    word_embedding_dict = {line.split()[0] for line in word_embedding_vocab}


#tops = [20e4, 15e4, 10e4, 5e4, 1e4]
tops = [1e4, 5e4, 10e4, 15e4, 20e4]
for top in tops:
    is_in = 0
    is_not_in = 0
    i = 0
    with open(dataset_vocab_file, "r") as dataset_vocab:
        for w in dataset_vocab:
            word = w.split()[0]

            if i < top:
                if word in word_embedding_dict:
                    is_in += 1
                else:
                    is_not_in += 1
            i += 1
        if not (is_in + is_not_in) == int(top):
            print(is_not_in+is_in)
            print("top: " + str(top))
        print(str(top)+" "+str(is_not_in))

