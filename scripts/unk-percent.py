import numpy as np
import os

unknown_token = '[UNK]'

decoding_folder = "/home/hvingelby/Workspace/thesis/results/see-mod-decoding/pointer-pretrained/decode_test_400maxenc_4beam_35mindec_100maxdec_ckpt-260000_a/"
decoded_folder = decoding_folder+"decoded"

num_smmrys_w_unk_token = 0
num_smmrys = 0

for file in os.listdir(decoded_folder):
    with open(decoded_folder+"/"+file, 'r') as f:
        decoded_string = f.read().replace('\n',' ')
        decoded_string = decoded_string.replace(' . ', '').replace(' .', '')

        num_smmrys += 1
        if unknown_token in decoded_string:
            num_smmrys_w_unk_token += 1

print("# summaries with unknown tokens: {}".format(num_smmrys_w_unk_token))
print("# summaries: {}".format(num_smmrys))
print("UNK-%: {}".format(np.round(float(num_smmrys_w_unk_token)/float(num_smmrys)*100, 3)))