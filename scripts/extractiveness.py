import os
from nltk import ngrams
from collections import Counter
import numpy as np

test_file = "/home/hvingelby/Workspace/thesis/data/cnn-dm/test.txt"

decoding_folder = "/home/hvingelby/Workspace/thesis/results/see-mod-decoding/pretrained/decode_test_400maxenc_4beam_35mindec_100maxdec_ckpt-280000/"
decoded_folder = decoding_folder+"decoded"
reference_folder = decoded_folder+"reference"

results = [[], [], [], [], []] # 1-, 2-, 3- and 4-grams and sentence

ns = range(1,5)
num_files = 0

original_texts = []
with open(test_file, 'r') as file:
    for line in file:
        article_sents = line.split('abstract=b')[0].replace('article=b', '')

        abstract_sents = line.split('abstract=b')[1].split("</s>")
        abstract_sents = [abstract.replace("<s>", '') for abstract in abstract_sents if "\n" not in abstract]

        original_texts.append(article_sents)

for file in os.listdir(decoded_folder):
    with open(decoded_folder+"/"+file, 'r') as f:
        decoded_string = f.read().replace('\n',' ')
        decoded_string = decoded_string.replace(' . ', '').replace(' .', '')

        index = int(str(file).split("_")[0])

        original_text = original_texts[index]
        decoded_original_text = original_text.replace(' . ', '').replace(' .', '')

        for n in ns:
            n_grams = ngrams(decoded_string.split(), n)
            n_grams = set(n_grams)

            original_texts_n_grams = ngrams(decoded_original_text.split(), n)
            original_texts_n_grams = set(original_texts_n_grams)

            intersect = set.intersection(n_grams, original_texts_n_grams)

            intersection_len = len(intersect)


            len_unique_n_grams_decoded = len(n_grams)
            len_unique_n_grams_article = len(n_grams)

            percent_reused_n_grams = float(intersection_len) / float(len_unique_n_grams_article)
            results[n-1].append(percent_reused_n_grams)

    num_files += 1

print("1-gram percentage reused: {}".format(np.round(np.mean(results[0])*100, 3)))
print("2-gram percentage reused: {}".format(np.round(np.mean(results[1])*100, 3)))
print("3-gram percentage reused: {}".format(np.round(np.mean(results[2])*100, 3)))
print("4-gram percentage reused: {}".format(np.round(np.mean(results[3])*100, 3)))
print("#decoded_references: {}".format(num_files))
print("#original_articles: {}".format(len(original_texts)))